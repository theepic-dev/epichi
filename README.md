# EpicHi

Extra highlighting queries for neovim's TreeSitter.

This allows the use of some extra functionality, such as highlighting python imported modules.

## Rationale

Neovim / TreeSitter lacked the functionality when I wrote this query. It may be deprecated at some point. This will be updated on an "as-needed" basis and is a very low-priority project.
